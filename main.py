from src.turingtoy import run_turing_machine


def to_dict(keys, value):
    return {key: value for key in keys}


machine = {
    "blank": " ",
    "start state": "start",
    "final states": ["done"],
    "table": {
        # Prefix the input with a '+', and go to the rightmost digit.
        "start": {
            **to_dict(["0", "1"], {"L": "init"}),
        },
        "init": {
            " ": {"write": "+", "R": "right"},
        },
        "right": {
            **to_dict(["0", "1", "*"], "R"),
            " ": {"L": "readB"},
        },
        # Read and erase the last digit of the multiplier.
        # If it's 1, add the current multiplicand.
        # In any case, double the multiplicand afterwards.
        "readB": {
            "0": {"write": " ", "L": "doubleL"},
            "1": {"write": " ", "L": "addA"},
        },
        "addA": {
            **to_dict(["0", "1"], "L"),
            "*": {"L": "read"},  # enter adder
        },
        # Double the multiplicand by appending a 0.
        "doubleL": {
            **to_dict(["0", "1"], "L"),
            "*": {"write": "0", "R": "shift"},
        },
        "double": {  # return from adder
            **to_dict(["0", "1", "+"], "R"),
            "*": {"write": "0", "R": "shift"},
        },
        # Make room by shifting the multiplier right 1 cell.
        "shift": {
            "0": {"write": "*", "R": "shift0"},
            "1": {"write": "*", "R": "shift1"},
            " ": {"L": "tidy"},  # base case: multiplier = 0
        },
        "shift0": {
            "0": {"R": "shift0"},
            "1": {"write": "0", "R": "shift1"},
            " ": {"write": "0", "R": "right"},
        },
        "shift1": {
            "0": {"write": "1", "R": "shift0"},
            "1": {"R": "shift1"},
            " ": {"write": "1", "R": "right"},
        },
        "tidy": {
            **to_dict(["0", "1"], {"write": " ", "L": "tidy"}),
            "+": {"write": " ", "L": "done"},
        },
        "done": {},
        # This is the 'binary addition' machine almost verbatim.
        # It's adjusted to keep the '+'
        # and to lead to another state instead of halting.
        "read": {
            "0": {"write": "c", "L": "have0"},
            "1": {"write": "c", "L": "have1"},
            "+": {"L": "rewrite"},  # keep the +
        },
        "have0": {**to_dict(["0", "1"], "L"), "+": {"L": "add0"}},
        "have1": {**to_dict(["0", "1"], "L"), "+": {"L": "add1"}},
        "add0": {
            **to_dict(["0", " "], {"write": "O", "R": "back0"}),
            "1": {"write": "I", "R": "back0"},
            **to_dict(["O", "I"], "L"),
        },
        "add1": {
            **to_dict(["0", " "], {"write": "I", "R": "back1"}),
            "1": {"write": "O", "L": "carry"},
            **to_dict(["O", "I"], "L"),
        },
        "carry": {
            **to_dict(["0", " "], {"write": "1", "R": "back1"}),
            "1": {"write": "0", "L": "carry"},
        },
        "back0": {
            **to_dict(["0", "1", "O", "I", "+"], "R"),
            "c": {"write": "0", "L": "read"},
        },
        "back1": {
            **to_dict(["0", "1", "O", "I", "+"], "R"),
            "c": {"write": "1", "L": "read"},
        },
        "rewrite": {
            "O": {"write": "0", "L": "rewrite"},
            "I": {"write": "1", "L": "rewrite"},
            **to_dict(["0", "1"], "L"),
            " ": {"R": "double"},  # when done, go to the 'double' state
        },
    },
}

input_ = "11*101"
output, execution_history, accepted = run_turing_machine(machine, input_)
assert output == "1111"
