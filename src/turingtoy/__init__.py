from time import sleep
from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    """Run a Turing machine.

    Args:
        machine: A dictionary representing a Turing machine.
        input_: The input string to the Turing machine.
        steps: The maximum number of steps to run the Turing machine.

    Returns:
        A tuple containing the final state of the Turing machine, the tape, and whether the machine halted.
    """
    memory = list(input_)
    state = machine["start state"]
    position = 0
    reading = memory[position]
    transition = machine["table"][state][reading]
    halted = False
    history = [{
        "state": state,
        "reading": memory[position],
        "position": position,
        "memory": "".join(memory),
        "transition": transition,
    }]

    while machine["final states"] and state not in machine["final states"]:
        trace = {}

        # The current state is not in the table
        if state not in machine["table"]:
            halted = True
            memory = list(f"Invalid state: {state}")
            break

        trace["state"] = state

        # Read the current symbol
        try:
            reading = memory[position]
        except IndexError:
            reading = machine["blank"]
            memory.append(reading)

        trace["reading"] = reading

        # The current symbol is not in the table
        if reading not in machine["table"][state]:
            halted = True
            memory = list(f"Invalid symbol: `{reading}` @(p:{position})")
            break

        # Get the transition from the table
        transition = machine["table"][state][reading]

        trace["position"] = position
        trace["memory"] = "".join(memory)
        trace["transition"] = transition

        # Update the state of the Turing machine
        memory, state, position = update_state(state, transition, position, memory, machine["blank"])

        # Append the current state of the Turing machine to the history
        history.append(trace)

    memory = clean_memory(memory, machine["blank"])

    return "".join(memory), history, not halted


def clean_memory(memory: List[str], blank: str = " ") -> List[str]:
    """Remove leading and trailing blanks from the memory.

    Args:
        memory: The memory of a Turing machine.
        blank: The blank symbol of the Turing machine.

    Returns:
        The memory with leading and trailing blanks removed.
    """
    while memory and memory[0] == blank:
        memory.pop(0)

    while memory and memory[-1] == blank:
        memory.pop()

    return memory


def update_state(
    state: str,
    transition: Dict | str,
    position: int,
    memory: List[str],
    blank: str = " ",
) -> Tuple[List[str], str, int]:
    """Update the state of a Turing machine.

    Args:
        machine: A dictionary representing a Turing machine.
        state: The current state of the Turing machine.
        tape: The tape of the Turing machine.
        position: The current position of the Turing machine.

    Returns:
        A tuple containing the new state of the Turing machine, the new tape, and the new position and whether the machine halted.
    """

    # If the transition contains a write operation, update the tape
    if "write" in transition and position < len(memory):
        memory[position] = transition["write"]

    if isinstance(transition, dict):
        position = position + 1 if "R" in transition else position - 1
        state = transition.get("R") if "R" in transition else transition.get("L")

    if isinstance(transition, str):
        position = position + 1 if transition == "R" else position - 1

    if position < 0:
        memory.insert(0, blank)
        position = 0

    return memory, state, position
